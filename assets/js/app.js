// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import css from "../css/app.scss"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import dependencies
//
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

// import socket from "./socket"
//

window.confirmInputsMatch = function(input, confirmation, message) {
  if (input.value != confirmation.value) {
    confirmation.setCustomValidity(message);
  } else {
    confirmation.setCustomValidity('');
  }
}
window.passwordConfirmationValidation = function(password, confirmation) {
  window.confirmInputsMatch(
    document.getElementById(password),
    document.getElementById(confirmation),
    "Password does not match confirmation"
  );
}
