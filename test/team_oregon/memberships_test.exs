defmodule TeamOregon.MembershipsTest do
  use TeamOregon.DataCase

  alias TeamOregon.Memberships
  alias TeamOregon.Memberships.{Membership, MembershipType}

  describe "create_membership_type/1" do
    test "with valid data it returns {:ok, membership_type}" do
      %{name: "Test Membership Type", price: 1.00, approval_required: false}
      |> Memberships.create_membership_type()
      |> (&assert({:ok, %MembershipType{}} = &1)).()
    end
  end

  describe "complete_purchase/3" do
    setup do
      user = user_fixture()
      membership_type = membership_type_fixture()
      payment_data = payment_data_fixture()

      membership =
        Memberships.complete_purchase(user, membership_type, payment_data)

      {:ok,
       %{
         user: user,
         membership_type: membership_type,
         payment_data: payment_data,
         membership: membership
       }}
    end

    test "it returns a membership", %{membership: membership} do
      assert %Memberships.Membership{} = membership
    end

    test "the membership belongs to the user", %{
      user: user,
      membership: membership
    } do
      assert user == Repo.preload(membership, :user).user
    end
  end

  describe "current_membership/2" do
    test "returns nil when the user is nil" do
      assert nil == Memberships.current_membership(nil)
    end

    test "returns nil when there are no user memberships" do
      user = user_fixture()

      assert nil == Memberships.current_membership(user)
    end

    test "returns nil when no memberships cover the specified time" do
      now = TeamOregon.Clock.now()
      user = user_fixture()

      # expires before now
      membership_fixture(%{
        user: user,
        starts_at: Timex.shift(now, years: -1),
        expires_at: Timex.shift(now, microseconds: -1)
      })

      # starts after now
      membership_fixture(%{
        user: user,
        starts_at: Timex.shift(now, microseconds: 1)
      })

      assert nil == Memberships.current_membership(user, now)
    end

    test "returns membership that covers the specified time" do
      now = TeamOregon.Clock.now()
      user = user_fixture()

      # expires before now
      membership_fixture(%{
        user: user,
        starts_at: Timex.shift(now, years: -2),
        expires_at: Timex.shift(now, years: -1)
      })

      # starts after now
      membership_fixture(%{
        user: user,
        starts_at: Timex.shift(now, years: 1),
        expires_at: Timex.shift(now, years: 2)
      })

      %{id: current_membership_id} =
        membership_fixture(%{
          user: user,
          starts_at: Timex.shift(now, years: -1),
          expires_at: Timex.shift(now, years: 1)
        })

      assert %Membership{id: ^current_membership_id} =
               Memberships.current_membership(user, now)
    end
  end
end
