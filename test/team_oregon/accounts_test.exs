defmodule TeamOregon.AccountsTest do
  use TeamOregon.DataCase
  use Bamboo.Test
  import TeamOregon.TestHelpers

  alias TeamOregon.Accounts

  describe "registration_form/0" do
    test "returns an Ecto.Changeset for a User" do
      assert %Ecto.Changeset{data: %Accounts.User{}} =
               Accounts.registration_form()
    end
  end

  describe "registration_form/1" do
    test "returns an Ecto.Changeset for a User" do
      assert %Ecto.Changeset{data: %Accounts.User{}} =
               Accounts.registration_form(%{email_address: "nobody@example.com"})
    end
  end

  describe "register_user/1 with valid registration data" do
    @valid_user_registration %{
      name: "John Doe",
      display_name: "John D.",
      email_address: unique_id() <> "johndoe@example.com",
      credential: %{
        password: "This is a password",
        password_confirmation: "This is a password"
      },
      accepts_code_of_conduct: true
    }

    test "it returns the user" do
      assert {:ok, %Accounts.User{}} =
               Accounts.register_user(@valid_user_registration)
    end

    test "it adds the user to the repository" do
      {:ok, %{id: user_id}} = Accounts.register_user(@valid_user_registration)
      refute nil == user_id

      assert %Accounts.User{} =
               user = TeamOregon.Repo.get(Accounts.User, user_id)

      assert @valid_user_registration.name == user.name
      assert @valid_user_registration.email_address == user.email_address
      assert @valid_user_registration.display_name == user.display_name
    end

    test "it sends the email confirmation instructions" do
      {:ok, user} = Accounts.register_user(@valid_user_registration)

      assert_email_delivered_with(
        subject: "Please Confirm Your Email Address",
        to: [{user.name, user.email_address}],
        text_body: ~r/confirmation code is: #{user.email_confirmation_code}/
      )
    end
  end

  describe "register_user/1 with invalid registration data" do
    @invalid_user_registration %{}

    test "it returns the invalid registration data as an error" do
      assert {:error, %Ecto.Changeset{changes: @invalid_user_registration}} =
               Accounts.register_user(@invalid_user_registration)
    end
  end

  describe "confirm_email_address/2" do
    test "it returns an error if there is no user with that email address" do
      assert {:error, "incorrect email address and/or confirmation code"} =
               Accounts.confirm_email_address(
                 "bad-email@example.com",
                 "doesn't matter"
               )
    end

    test "it returns an error if the confirmation code is not correct" do
      user = user_fixture()

      assert {:error, "incorrect email address and/or confirmation code"} =
               Accounts.confirm_email_address(user.email_address, "bad code")
    end

    test "it returns the user account if the confirmation code is correct" do
      %{id: user_id, email_address: email, email_confirmation_code: code} =
        user_fixture()

      assert {:ok, %Accounts.User{id: ^user_id}} =
               Accounts.confirm_email_address(email, code)
    end
  end

  describe "email_is_confirmed?/1" do
    test "is false when the email address does not belong to an existing user" do
      refute Accounts.email_is_confirmed?("bad-email@example.com")
    end

    test "is false when a user has the email address but has not confirmed it" do
      %{email_address: email} = user_fixture()
      refute Accounts.email_is_confirmed?(email)
    end

    test "is true when the user has confirmed their email address" do
      %{email_address: email, email_confirmation_code: code} = user_fixture()
      {:ok, _user} = Accounts.confirm_email_address(email, code)
      assert Accounts.email_is_confirmed?(email)
    end
  end

  describe "request_confirmation_email/1" do
    test "returns :already_confirmed when email belongs to confirmed user" do
      %{email_address: email, email_confirmation_code: code} = user_fixture()
      {:ok, _} = Accounts.confirm_email_address(email, code)
      assert :already_confirmed = Accounts.request_confirmation_email(email)
    end

    test "returns :ok when email belongs to unconfirmed user" do
      %{email_address: email} = user_fixture()
      assert :ok = Accounts.request_confirmation_email(email)
    end

    test "sends the confirmation email when email belongs to unconfirmed user" do
      %{name: user_name, email_address: email} = user_fixture()
      # clear email queue from user creation
      assert_email_delivered_with(%{})
      Accounts.request_confirmation_email(email)

      assert_email_delivered_with(
        subject: "Please Confirm Your Email Address",
        to: [{user_name, email}]
      )
    end
  end
end
