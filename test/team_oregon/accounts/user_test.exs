defmodule TeamOregon.Accounts.UserTest do
  use TeamOregon.DataCase

  alias TeamOregon.Accounts.User

  describe "registration_changeset/1" do
    test "sets the email confirmation code for the user" do
      input = %{email_address: "john.doe@example.com"}

      expected_code =
        :crypto.hash(
          :md5,
          TeamOregon.get_config(:secret_key_base) <> input.email_address
        )
        |> Base.encode16()

      user =
        User.registration_changeset(input) |> Ecto.Changeset.apply_changes()

      assert %{email_confirmation_code: ^expected_code} = user
    end
  end
end
