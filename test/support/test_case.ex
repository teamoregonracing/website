defmodule TeamOregon.TestCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      import TeamOregon.TestCase
    end
  end

  @doc """
  A helper that transforms changeset errors into a map of messages.

      assert {:error, changeset} = Accounts.create_user(%{password: "short"})
      assert "password is too short" in errors_on(changeset).password
      assert %{password: ["password is too short"]} = errors_on(changeset)

  """
  def errors_on(changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {message, opts} ->
      Enum.reduce(opts, message, fn {key, value}, acc ->
        String.replace(acc, "%{#{key}}", to_string(value))
      end)
    end)
  end

  def errors_on(changeset, field) do
    case errors_on(changeset) do
      %{^field => errors} -> errors
      _ -> []
    end
  end
end
