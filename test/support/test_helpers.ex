defmodule TeamOregon.TestHelpers do
  alias TeamOregon.{Accounts, Memberships}

  def unique_id do
    to_string(System.unique_integer())
  end

  def user_fixture(attrs \\ %{}) do
    unique_user_id = Map.get(attrs, :user_id, unique_id())

    {:ok, user} =
      attrs
      |> Enum.into(%{
        name: "John Doe",
        display_name: to_string(unique_user_id),
        email_address: "#{unique_user_id}@example.com",
        accepts_code_of_conduct: true,
        credential: %{
          password: "User Fixture Password",
          password_confirmation: "User Fixture Password"
        }
      })
      |> Accounts.register_user()

    user
  end

  def membership_type_fixture(attrs \\ %{}) do
    unique_id = Map.get(attrs, :membership_type_id, unique_id())

    {:ok, membership_type} =
      attrs
      |> Enum.into(%{
        name: "Test Member Type - #{unique_id}",
        price: 1.00,
        approval_required: false
      })
      |> Memberships.create_membership_type()

    membership_type
  end

  def payment_data_fixture(attrs \\ %{}) do
    unique_payment_id = Map.get(attrs, :payment_id, "PAY-" <> unique_id())

    attrs
    |> Enum.into(%{
      "id" => unique_payment_id,
      "create_time" => "2017-09-22T20:53:43Z",
      "update_time" => "2017-09-22T20:53:44Z",
      "state" => "CREATED",
      "intent" => "sale",
      "payer" => %{
        "payment_method" => "paypal"
      },
      "transactions" => [
        %{
          "amount" => %{
            "total" => "65.00",
            "currency" => "USD"
          },
          "description" => "Annual Membership",
          "item_list" => %{
            "items" => [
              %{
                "name" => "Standard Membership",
                "sku" => "1",
                "price" => "65.00",
                "currency" => "USD",
                "quantity" => "1"
              }
            ],
            "shipping_address" => %{
              "recipient_name" => "Brian Robinson",
              "line1" => "4th Floor",
              "line2" => "Unit #34",
              "city" => "San Jose",
              "state" => "CA",
              "phone" => "011862212345678",
              "postal_code" => "95131",
              "country_code" => "US"
            }
          }
        }
      ]
    })
  end

  def membership_fixture(attrs \\ %{}) do
    attrs = %{
      user: attrs.user || user_fixture(),
      membership_type: attrs[:membership_type] || membership_type_fixture(),
      payment_data: attrs[:payment_data] || payment_data_fixture(),
      starts_at: attrs[:starts_at] || TeamOregon.Clock.now(),
      expires_at: attrs[:expires_at] || one_year_from_now()
    }

    case Memberships.create_membership(attrs) do
      {:ok, membership} -> membership
    end
  end

  defp one_year_from_now() do
    TeamOregon.Clock.now()
    |> Timex.shift(years: 1)
  end
end
