defmodule TeamOregonWeb.ConnCase do
  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection.

  Such tests rely on `Phoenix.ConnTest` and also
  import other functionality to make it easier
  to build common data structures and query the data layer.

  Finally, if the test case interacts with the database,
  it cannot be async. For this reason, every test runs
  inside a transaction which is reset at the beginning
  of the test unless the test case is marked as async.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      # Import conveniences for testing with connections
      use Phoenix.ConnTest
      alias TeamOregonWeb.Router.Helpers, as: Routes

      # The default endpoint for testing
      @endpoint TeamOregonWeb.Endpoint

      import TeamOregonWeb.ConnCase

      import TeamOregon.TestHelpers
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(TeamOregon.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(TeamOregon.Repo, {:shared, self()})
    end

    {:ok, conn: Phoenix.ConnTest.build_conn()}
  end

  def html_response(conn, status, template) do
    body = Phoenix.ConnTest.html_response(conn, status)

    # These matches are broken out so that the error messages for a failed match
    # are easier to read.
    %{private: private} = conn
    %{phoenix_template: actual_template} = private
    ^template = actual_template

    HtmlEntities.decode(body)
  end
end
