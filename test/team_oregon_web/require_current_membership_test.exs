defmodule TeamOregonWeb.RequireCurrentMembershipTest do
  use TeamOregonWeb.ConnCase
  use Plug.Test

  test "when the user is not signed it, it redirects to the sign in page", %{
    conn: conn
  } do
    conn
    |> call_plug()
    |> (&assert(redirected_to(&1) == Routes.session_path(&1, :sign_in) && &1)).()
    |> (&assert(&1.halted)).()
  end

  test "when the user is signed in and has a current membership, it does not redirect or halt",
       %{conn: conn} do
    conn
    |> set_current_user_with_membership()
    |> call_plug()
    |> (&(assert(%{state: :unset} = &1) && &1)).()
    |> (&refute(&1.halted)).()
  end

  test "when the user is signed in and does not have a current membership, it redirects to the purchase membership page",
       %{conn: conn} do
    conn
    |> assign(:current_user, user_fixture())
    |> call_plug()
    |> (&(assert(
            redirected_to(&1) ==
              Routes.registration_path(&1, :purchase_membership)
          ) && &1)).()
    |> (&assert(&1.halted)).()
  end

  defp set_current_user_with_membership(conn) do
    conn
    |> assign(:current_user, user_with_membership())
  end

  defp user_with_membership do
    user = user_fixture()
    membership_fixture(%{user: user})
    user
  end

  defp call_plug(conn) do
    conn
    |> bypass_through(TeamOregonWeb.Router, [:browser])
    |> get("/")
    |> TeamOregonWeb.RequireCurrentMembership.call(%{})
  end
end
