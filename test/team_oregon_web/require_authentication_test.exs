defmodule TeamOregonWeb.RequireAuthenticationTest do
  use TeamOregonWeb.ConnCase
  use Plug.Test

  test "when the user is not signed it, it redirects to the sign in page", %{
    conn: conn
  } do
    conn
    |> call_plug()
    |> (&assert(redirected_to(&1) == Routes.session_path(&1, :sign_in) && &1)).()
    |> (&assert(&1.halted)).()
  end

  test "when the user is signed in it does not redirect or halt", %{conn: conn} do
    conn
    |> assign(:current_user, user_fixture())
    |> call_plug()
    |> (&(assert(%{state: :unset} = &1) && &1)).()
    |> (&refute(&1.halted)).()
  end

  defp call_plug(conn) do
    conn
    |> bypass_through(TeamOregonWeb.Router, [:browser])
    |> get("/")
    |> TeamOregonWeb.RequireAuthentication.call(%{})
  end
end
