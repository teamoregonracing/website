defmodule TeamOregonWeb.AuthTest do
  use TeamOregonWeb.ConnCase
  use Plug.Test

  alias TeamOregon.Accounts.User

  test "when current_user is already in assigns it keeps that current user", %{
    conn: conn
  } do
    user = %User{id: 12345}

    conn
    |> assign(:current_user, user)
    |> call_plug()
    |> (&assert(%User{id: 12345} = &1.assigns[:current_user])).()
  end

  test "when no current_user or session user_id it does not set current_user",
       %{conn: conn} do
    conn
    |> call_plug()
    |> (&assert(nil == &1.assigns[:current_user])).()
  end

  test "when user_id set in session is a real user, that user is set as current_user",
       %{conn: conn} do
    %User{id: user_id} = user_fixture()

    conn
    |> init_test_session(user_id: user_id)
    |> call_plug()
    |> (&assert(%User{id: ^user_id} = &1.assigns[:current_user])).()
  end

  test "when current_user does not match existing user_id in session, user_id is updated",
       %{conn: conn} do
    user = %User{id: 12345}

    conn
    |> init_test_session(user_id: 67890)
    |> assign(:current_user, user)
    |> call_plug()
    |> (&assert(12345 = get_session(&1, :user_id))).()
  end

  test "when current_user does not match existing user_id in session, current_user stays the same",
       %{conn: conn} do
    user = %User{id: 12345}

    conn
    |> init_test_session(user_id: 67890)
    |> assign(:current_user, user)
    |> call_plug()
    |> (&assert(^user = &1.assigns[:current_user])).()
  end

  defp call_plug(conn) do
    conn
    |> bypass_through(TeamOregonWeb.Router, [:browser])
    |> get("/")
  end
end
