defmodule TeamOregonWeb.RegistrationControllerTest do
  use TeamOregonWeb.ConnCase
  use Bamboo.Test

  alias TeamOregon.Accounts

  describe "new/2" do
    test "it renders new registration form with 200 status", %{conn: conn} do
      conn = get(conn, Routes.registration_path(conn, :new))
      assert html_response(conn, 200, "new.html")
    end
  end

  describe "create/2" do
    @valid_reg %{
      name: "John Doe",
      display_name: "John D.",
      email_address: unique_id() <> "johndoe@example.com",
      credential: %{
        password: "I am an OK password, I guess",
        password_confirmation: "I am an OK password, I guess"
      },
      accepts_code_of_conduct: true
    }

    test "with invalid registration, it renders new registration form with " <>
           "400 status",
         %{conn: conn} do
      invalid_reg = %{user: %{}}
      conn = post(conn, Routes.registration_path(conn, :create), invalid_reg)
      assert html_response(conn, 400, "new.html")
    end

    test "with valid registration, it renders email confirmation form with " <>
           "201 status",
         %{conn: conn} do
      conn =
        post(conn, Routes.registration_path(conn, :create), %{user: @valid_reg})

      assert redirected_to(conn) ==
               Routes.registration_path(conn, :input_email_confirmation_code)
    end

    test "with valid registration, it logs the user in", %{conn: conn} do
      conn =
        post(conn, Routes.registration_path(conn, :create), %{user: @valid_reg})

      assert get_session(conn, :user_id)
    end

    test "with valid registraton, it sends a confirmation email", %{conn: conn} do
      post(conn, Routes.registration_path(conn, :create), %{user: @valid_reg})

      assert_email_delivered_with(
        subject: "Please Confirm Your Email Address",
        to: [{@valid_reg.name, @valid_reg.email_address}]
      )
    end
  end

  describe "input_email_confirmation_code/2 as anonymous user" do
    test "it redirects to the login page when the user is not logged in",
         %{conn: conn} do
      conn =
        get(
          conn,
          Routes.registration_path(conn, :input_email_confirmation_code)
        )

      assert redirected_to(conn) == Routes.session_path(conn, :sign_in)
    end
  end

  describe "input_email_confirmation_code/2 as logged in user" do
    setup %{conn: conn, current_user: user_id} do
      user = user_fixture(%{user_id: user_id})
      conn = assign(conn, :current_user, user)
      {:ok, conn: conn, user: user}
    end

    @tag current_user: "john"
    test "it renders the confirm_email page", %{conn: conn} do
      conn =
        get(
          conn,
          Routes.registration_path(conn, :input_email_confirmation_code)
        )

      assert html_response(conn, 200, "confirm_email.html")
    end
  end

  describe "verify_email_confirmation_code/2 as anonymous user" do
    test "it redirects to the login page when the user is not logged in",
         %{conn: conn} do
      conn =
        post(
          conn,
          Routes.registration_path(conn, :verify_email_confirmation_code)
        )

      assert redirected_to(conn) == Routes.session_path(conn, :sign_in)
    end
  end

  describe "verify_email_confirmation_code/2 as a logged in user" do
    setup %{conn: conn, current_user: user_id} do
      user = user_fixture(%{user_id: user_id})
      conn = assign(conn, :current_user, user)
      {:ok, conn: conn, user: user}
    end

    @tag current_user: "john"
    test "it renders the confirm_email page when the user has not already confirmed their email address and an incorrect confirmation code is provided",
         %{conn: conn} do
      conn =
        post(
          conn,
          Routes.registration_path(conn, :verify_email_confirmation_code,
            confirmation_code: "bad code"
          )
        )

      assert html_response(conn, 400, "confirm_email.html")
    end

    @tag current_user: "john"
    test "it redirects to the homepage when the correct confirmation code is provided and user has a current membership",
         %{conn: conn, user: user} do
      membership_fixture(%{user: user})

      conn =
        post(
          conn,
          Routes.registration_path(conn, :verify_email_confirmation_code,
            confirmation_code: user.email_confirmation_code
          )
        )

      assert redirected_to(conn) == Routes.page_path(conn, :index)
    end

    @tag current_user: "john"
    test "it redirects to the purchase membership page when the correct confirmation code is provided and the user does not have a current membership",
         %{conn: conn, user: user} do
      conn =
        post(
          conn,
          Routes.registration_path(conn, :verify_email_confirmation_code,
            confirmation_code: user.email_confirmation_code
          )
        )

      assert redirected_to(conn) ==
               Routes.registration_path(conn, :purchase_membership)
    end
  end

  describe "request_confirmation_email/2 as anonymous user" do
    test "it redirects to the login page when the user is not logged in",
         %{conn: conn} do
      conn =
        post(conn, Routes.registration_path(conn, :request_confirmation_email))

      assert redirected_to(conn) == Routes.session_path(conn, :sign_in)
    end
  end

  describe "request_confirmation_email/2 as logged in user with confirmed email" do
    setup %{conn: conn, current_user: user_id} do
      user = user_fixture(%{user_id: user_id})

      {:ok, _} =
        Accounts.confirm_email_address(
          user.email_address,
          user.email_confirmation_code
        )

      conn = assign(conn, :current_user, user)
      {:ok, conn: conn, user: user}
    end

    @tag current_user: "john"
    test "it redirects to the home page", %{conn: conn, user: user} do
      conn =
        post(conn, Routes.registration_path(conn, :request_confirmation_email))

      assert redirected_to(conn) == Routes.page_path(conn, :index)

      assert get_flash(conn, :info) ==
               "Your email address, #{user.email_address}, has already been confirmed."
    end
  end

  describe "request_confirmation_email/2 as logged in user with unconfirmed email" do
    setup %{conn: conn, current_user: user_id} do
      user = user_fixture(%{user_id: user_id})
      conn = assign(conn, :current_user, user)
      # Creating the user will deliver an email, so we need to clear that first
      # one out of the queue in order to test for subsequent email deliveries.
      assert_email_delivered_with(%{})
      {:ok, conn: conn, user: user}
    end

    @tag current_user: "john"
    test "redirects to the confirm email page", %{conn: conn, user: user} do
      conn =
        post(conn, Routes.registration_path(conn, :request_confirmation_email))

      assert redirected_to(conn) ==
               Routes.registration_path(conn, :input_email_confirmation_code)

      assert get_flash(conn, :info) ==
               "A confirmation email has been sent to #{user.email_address}."
    end

    @tag current_user: "john"
    test "sends a confirmation email to the user's email address", %{
      conn: conn,
      user: user
    } do
      post(conn, Routes.registration_path(conn, :request_confirmation_email))

      assert_email_delivered_with(
        subject: "Please Confirm Your Email Address",
        to: [{user.name, user.email_address}]
      )
    end
  end
end
