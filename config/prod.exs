use Mix.Config
config :logger, level: :info

config :team_oregon, TeamOregonWeb.Endpoint,
  load_from_system_env: true,
  # Without this line, your app will not start the web server!
  server: true,
  cache_static_manifest: "priv/static/cache_manifest.json",
  secret_key_base: "${SECRET_KEY_BASE}",
  force_ssl: [rewrite_on: [:x_forwarded_proto]]

config :team_oregon, TeamOregon.Repo,
  adapter: Ecto.Adapters.Postgres,
  url: "${DATABASE_URL}",
  database: "",
  ssl: true,
  pool_size: 10

config :team_oregon, TeamOregon.Mailer,
  adapter: Bamboo.SendGridAdapter,
  api_key: {:system, "SENDGRID_API_KEY"}
