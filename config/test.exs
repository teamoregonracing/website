use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :team_oregon, TeamOregonWeb.Endpoint,
  http: [port: 4002],
  url: [host: "localhost", port: 4002],
  server: false,
  secret_key_base:
    "3PXN/6k6qoxqQjWFskGew4r74yp7oJ1UNF6wjvJSHjC5Y5LLIrDpWxrJ84UBphJn"

# Print only warnings and errors during test
config :logger, level: :warn

config :team_oregon,
  secret_key_base:
    "3PXN/6k6qoxqQjWFskGew4r74yp7oJ1UNF6wjvJSHjC5Y5LLIrDpWxrJ84UBphJn"

# Configure your database
config :team_oregon, TeamOregon.Repo,
  username: System.get_env("POSTGRES_USER") || "phoenix",
  password: System.get_env("POSTGRES_PASSWORD") || "rising",
  database: System.get_env("POSTGRES_DB") || "team_oregon_test",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

config :team_oregon, TeamOregon.Mailer, adapter: Bamboo.TestAdapter

config :pbkdf2_elixir, rounds: 1
