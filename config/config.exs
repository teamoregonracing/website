# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# run shell command to "source .env" to load the environment variables.
# wrap in "try do"
try do
  # in case .env file does not exist.
  File.stream!("./.env")
  # remove excess whitespace
  |> Stream.map(&String.trim_trailing/1)
  # loop through each line
  |> Enum.each(fn line ->
    line
    # remove "export" from line
    |> String.replace("export ", "")
    # split on *first* "=" (equals sign)
    |> String.split("=", parts: 2)
    # stackoverflow.com/q/33055834/1148249
    |> Enum.reduce(fn value, key ->
      # set each environment variable
      System.put_env(key, value)
    end)
  end)
rescue
  _ -> IO.puts("no .env file found!")
end

config :team_oregon,
  ecto_repos: [TeamOregon.Repo],
  secret_key_base: {:system, "SECRET_KEY_BASE"}

# Configures the endpoint
config :team_oregon, TeamOregonWeb.Endpoint,
  http: [port: {:system, "PORT"}],
  url: [
    host: {:system, "TEAM_OREGON_WEB_HOST"},
    port: {:system, "TEAM_OREGON_WEB_PORT"}
  ],
  secret_key_base: System.get_env("SECRET_KEY_BASE"),
  render_errors: [view: TeamOregonWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: TeamOregon.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :team_oregon, TeamOregon.Mailer, adapter: Bamboo.LocalAdapter

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
