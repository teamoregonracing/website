defmodule TeamOregonWeb.LayoutView do
  alias TeamOregon.Accounts.User

  use TeamOregonWeb, :view

  def main_menu_link(conn, :home) do
    to = Routes.page_path(conn, :index)
    active = to == current_path(conn)
    render("main_menu_link.html", title: "Home", to: to, active: active)
  end

  def main_menu_link(conn, :join) do
    to = Routes.registration_path(conn, :new)
    active = to == current_path(conn)

    render("main_menu_link.html", title: "Join the Team", to: to, active: active)
  end

  def main_menu_link(conn, :sign_out) do
    to = Routes.session_path(conn, :sign_out)
    render("main_menu_link.html", title: "Sign Out", to: to, active: false)
  end

  def main_menu_link(conn, :sign_in) do
    to = Routes.session_path(conn, :sign_in)
    render("main_menu_link.html", title: "Sign In", to: to, active: false)
  end

  def main_menu_link(conn, id, unless: unless_cond) do
    if !unless_cond, do: main_menu_link(conn, id)
  end

  def main_menu_link(conn, id, if: if_cond) do
    if if_cond, do: main_menu_link(conn, id)
  end

  def render_messages(conn) do
    assigns =
      conn.assigns
      |> Map.merge(%{conn: conn})
      |> warn_on_expired_membership()
      |> extract_error_messages()
      |> extract_info_messages()

    render("messages.html", assigns)
  end

  defp warn_on_expired_membership(%{warn_membership_expired: _} = assigns),
    do: assigns

  defp warn_on_expired_membership(%{current_user: %User{} = user} = assigns) do
    display_warning = !TeamOregon.Memberships.has_current_membership?(user)
    Map.merge(assigns, %{warn_membership_expired: display_warning})
  end

  defp warn_on_expired_membership(assigns) do
    Map.merge(assigns, %{warn_membership_expired: false})
  end

  defp extract_error_messages(%{conn: conn} = assigns) do
    assigns
    |> Map.merge(
      case get_flash(conn, :error) do
        nil ->
          %{has_error_messages: false, error_messages: []}

        errors when is_list(errors) ->
          %{has_error_messages: true, error_messages: errors}

        errors ->
          %{has_error_messages: true, error_messages: [errors]}
      end
    )
  end

  defp extract_info_messages(%{conn: conn} = assigns) do
    assigns
    |> Map.merge(
      case get_flash(conn, :info) do
        nil ->
          %{has_info_messages: false, info_messages: []}

        infos when is_list(infos) ->
          %{has_info_messages: true, info_messages: infos}

        infos ->
          %{has_info_messages: true, info_messages: [infos]}
      end
    )
  end
end
