defmodule TeamOregonWeb.View.FormHelper do
  @moduledoc """
  Custom helper functions for HTML forms
  """

  use Phoenix.HTML

  def input(form, field, opts \\ [])

  def input(form, field, opts) do
    opts = build_input_opts(form, field, opts)

    content_tag :div, opts.wrapper_opts do
      form
      |> build_any_input(field, opts)
      |> add_help(opts)
    end
  end

  def input(form, field, opts, more_opts)
      when is_list(opts) and is_list(more_opts) do
    input(form, field, opts ++ more_opts)
  end

  def form_actions(_form, options \\ []) do
    content_tag :div, class: "form-action-buttons" do
      []
      |> add_cancel_button(options)
      |> add_submit_button(options)
    end
  end

  defp add_cancel_button(buttons, options) do
    options
    |> Keyword.get(:cancel)
    |> build_cancel_button()
    |> add_button_to(buttons)
  end

  defp build_cancel_button(nil), do: nil

  defp build_cancel_button(label) when is_binary(label) do
    link(label,
      class: "btn btn-outline-danger",
      to: {:javascript, "history.back() || (window.location.href = '/');"}
    )
  end

  defp add_submit_button(buttons, options) do
    options
    |> Keyword.get(:submit)
    |> build_submit_button()
    |> add_button_to(buttons)
  end

  defp build_submit_button(nil), do: nil

  defp build_submit_button(label) when is_binary(label) do
    submit(label, class: "btn btn-success")
  end

  defp add_button_to(nil, buttons), do: buttons
  defp add_button_to(button, buttons), do: buttons ++ [button]

  defp build_any_input(form, field, opts) do
    case Phoenix.HTML.Form.input_type(form, field) do
      :checkbox ->
        build_checkbox_input(form, field, opts)

      _ ->
        build_input(form, field, opts)
    end
  end

  defp add_help(content, opts) do
    help = opts[:do]

    if help do
      help = content_tag(:div, help, class: "help-block")
      [content] ++ [help]
    else
      content
    end
  end

  defp build_checkbox_input(_form, _field, opts) do
    wrapper =
      content_tag :div, class: "checkbox" do
        [opts.input, opts.label]
      end

    [wrapper, opts.error]
  end

  defp build_input(_form, _field, opts) do
    [opts.label, opts.input, opts.error]
  end

  defp build_input_opts(form, field, opts) do
    opts =
      opts
      |> Enum.into(%{})
      |> Map.put(:type, Phoenix.HTML.Form.input_type(form, field))
      |> Map.put(:label_text, Keyword.get(opts, :label_text, humanize(field)))
      |> Map.put(:help, Keyword.get(opts, :do))
      |> Map.put(:wrapper_opts, class: wrapper_classes(form, field))
      |> Map.put(:label_opts, class: label_classes(form, field))
      |> Map.put(:error, TeamOregonWeb.ErrorHelpers.error_tag(form, field))
      |> Map.put(
        :input_opts,
        Keyword.get(opts, :input_opts, []) ++
          [class: input_classes(form, field)] ++
          fixed_input_validations(form, field)
      )

    opts
    |> Map.put(
      :label,
      maybe_required_label(form, field, opts.label_text, opts.label_opts)
    )
    |> Map.put(
      :input,
      apply(Phoenix.HTML.Form, opts.type, [form, field, opts.input_opts])
    )
  end

  defp maybe_required_label(form, field, text, opts) do
    if is_field_required(form, field) do
      label(form, field, opts) do
        {:safe, abbr} = content_tag(:abbr, "*", title: "required")
        {:safe, [abbr, text]}
      end
    else
      label(form, field, opts) do
        {:safe, text}
      end
    end
  end

  defp wrapper_classes(form, field) do
    ["form-group"]
    |> add_required_class(form, field)
    |> add_error_class(form, field)
    |> Enum.join(" ")
  end

  defp label_classes(form, field) do
    ["control-label"]
    |> add_required_class(form, field)
    |> Enum.join(" ")
  end

  defp input_classes(form, field) do
    []
    |> add_form_control_class(form, field)
    |> add_required_class(form, field)
    |> Enum.join(" ")
  end

  defp add_form_control_class(classes, form, field) do
    type = Phoenix.HTML.Form.input_type(form, field)

    case type do
      :checkbox ->
        classes

      _ ->
        ["form-control" | classes]
    end
  end

  defp add_required_class(classes, form, field) do
    if is_field_required(form, field) do
      ["required" | classes]
    else
      classes
    end
  end

  defp add_error_class(classes, form, field) do
    if field_has_errors(form, field) do
      ["errors" | classes]
    else
      classes
    end
  end

  defp is_field_required(form, field) do
    form
    |> input_validations(field)
    |> Keyword.get(:required, false)
  end

  defp field_has_errors(form, field) do
    Enum.any?(Keyword.get_values(form.errors, field), fn error -> error end)
  end

  defp fixed_input_validations(form, field) do
    {_, validations} =
      form
      |> Phoenix.HTML.Form.input_validations(field)
      |> Keyword.get_and_update(:required, fn is_required ->
        new_value = if(is_required, do: "required", else: nil)
        {is_required, new_value}
      end)

    Keyword.delete(validations, :required, nil)
  end
end
