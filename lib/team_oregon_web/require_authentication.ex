defmodule TeamOregonWeb.RequireAuthentication do
  import Plug.Conn
  import Phoenix.Controller

  alias TeamOregonWeb.Router.Helpers, as: Routes

  def init(opts), do: opts

  def call(conn, _opts) do
    cond do
      conn.halted ->
        conn

      conn.assigns[:current_user] ->
        conn

      true ->
        conn
        |> put_flash(
          :error,
          "You must be logged in to access the requested page."
        )
        |> redirect(to: Routes.session_path(conn, :sign_in))
        |> halt()
    end
  end
end
