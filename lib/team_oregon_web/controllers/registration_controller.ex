defmodule TeamOregonWeb.RegistrationController do
  use TeamOregonWeb, :controller

  alias TeamOregon.{Accounts, Memberships}

  plug TeamOregonWeb.RequireAuthentication
       when action not in [:new, :create]

  plug :skip_expired_membership_warning

  def new(conn, _params) do
    conn
    |> assign(:new_user, Accounts.registration_form())
    |> render("new.html")
  end

  def create(conn, %{"user" => registration_details}) do
    case Accounts.register_user(registration_details) do
      {:ok, user} ->
        conn
        |> put_session(:user_id, user.id)
        |> redirect(
          to: Routes.registration_path(conn, :input_email_confirmation_code)
        )

      {:error, registration_form} ->
        conn
        |> assign(:new_user, registration_form)
        |> put_status(400)
        |> render("new.html")
    end
  end

  def input_email_confirmation_code(conn, _params) do
    email_address = current_user_email(conn)

    conn
    |> assign(:email_address, email_address)
    |> render("confirm_email.html")
  end

  def verify_email_confirmation_code(
        conn,
        %{"confirmation_code" => confirmation_code} = params
      ) do
    email_address = current_user_email(conn)

    case Accounts.confirm_email_address(email_address, confirmation_code) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "#{email_address} has been confirmed.")
        |> redirect_after_confirmation(user)

      {:error, message} ->
        conn
        |> put_flash(:error, message)
        |> put_status(400)
        |> input_email_confirmation_code(params)
    end
  end

  defp redirect_after_confirmation(conn, user) do
    if Memberships.has_current_membership?(user) do
      redirect(conn, to: Routes.page_path(conn, :index))
    else
      redirect(conn, to: Routes.registration_path(conn, :purchase_membership))
    end
  end

  def request_confirmation_email(conn, _params) do
    email_address = current_user_email(conn)

    case Accounts.request_confirmation_email(email_address) do
      :ok ->
        conn
        |> put_flash(
          :info,
          "A confirmation email has been sent to #{email_address}."
        )
        |> redirect(
          to: Routes.registration_path(conn, :input_email_confirmation_code)
        )

      :already_confirmed ->
        conn
        |> put_flash(
          :info,
          "Your email address, #{email_address}, has already been confirmed."
        )
        |> redirect(to: Routes.page_path(conn, :index))
    end
  end

  def purchase_membership(conn, _params) do
    conn
    |> render("purchase_membership.html")
  end

  defp current_user_email(conn) do
    conn.assigns
    |> Map.get(:current_user, %{})
    |> Map.get(:email_address)
  end

  defp skip_expired_membership_warning(conn, _opts) do
    conn
    |> assign(:warn_membership_expired, false)
  end
end
