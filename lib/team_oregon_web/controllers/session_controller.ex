defmodule TeamOregonWeb.SessionController do
  use TeamOregonWeb, :controller

  alias TeamOregon.Accounts

  def sign_out(conn, _params) do
    conn
    |> clear_session()
    |> put_flash(:info, "You have been signed out.")
    |> redirect(to: Routes.page_path(conn, :index))
  end

  def sign_in(conn, _params) do
    render(conn, "sign_in.html")
  end

  def create(conn, %{"email" => email_address, "password" => password}) do
    case Accounts.sign_in(email_address, password) do
      {:ok, user} ->
        conn
        |> put_session(:user_id, user.id)
        |> assign(:current_user, user)
        |> put_flash(:info, "Welcome back, #{user.display_name}!")
        |> redirect(to: Routes.page_path(conn, :index))

      :error ->
        conn
        |> put_flash(
          :error,
          "The email and/or password provided do not match an account."
        )
        |> redirect(to: Routes.session_path(conn, :sign_in))
    end
  end
end
