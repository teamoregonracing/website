defmodule TeamOregonWeb.Router do
  use TeamOregonWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug TeamOregonWeb.Auth
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  if Mix.env() == :dev do
    forward "/sent_emails", Bamboo.SentEmailViewerPlug
  end

  scope "/", TeamOregonWeb do
    pipe_through :browser

    get "/", PageController, :index

    get "/sign_in", SessionController, :sign_in
    post "/sign_in", SessionController, :create
    get "/sign_out", SessionController, :sign_out

    get "/registration", RegistrationController, :new
    post "/registration", RegistrationController, :create

    post "/registration/request_confirmation_email",
         RegistrationController,
         :request_confirmation_email

    get "/registration/confirm_email",
        RegistrationController,
        :input_email_confirmation_code

    post "/registration/confirm_email",
         RegistrationController,
         :verify_email_confirmation_code

    get "/registration/purchase_membership",
        RegistrationController,
        :purchase_membership
  end
end
