defmodule TeamOregonWeb.RequireCurrentMembership do
  import Plug.Conn
  import Phoenix.Controller

  alias TeamOregonWeb.Router.Helpers, as: Routes
  alias TeamOregon.Memberships

  def init(opts), do: opts

  def call(conn, _opts) do
    conn
    |> require_authentication()
    |> require_current_membership()
  end

  defp require_authentication(conn) do
    TeamOregonWeb.RequireAuthentication.call(conn, [])
  end

  defp require_current_membership(%{halted: true} = conn), do: conn

  defp require_current_membership(conn) do
    if current_user_has_current_membership?(conn) do
      conn
    else
      conn
      |> redirect(to: Routes.registration_path(conn, :purchase_membership))
      |> halt()
    end
  end

  defp current_user_has_current_membership?(%{assigns: %{current_user: user}}) do
    Memberships.has_current_membership?(user)
  end

  defp current_user_has_current_membership?(_), do: false
end
