defmodule TeamOregon.Memberships do
  alias __MODULE__.{Membership, MembershipType}
  alias TeamOregon.Accounts.User
  alias TeamOregon.Repo

  import Ecto.Query

  def create_membership_type(attrs) do
    attrs
    |> MembershipType.changeset()
    |> Repo.insert()
  end

  def create_membership(attrs) do
    attrs
    |> Membership.changeset()
    |> Repo.insert()
  end

  def complete_purchase(user, membership_type, payment_data) do
    Membership.changeset(%{
      user: user,
      membership_type: membership_type,
      payment_data: payment_data
    })
    |> Ecto.Changeset.apply_changes()
  end

  def current_membership(user, now \\ TeamOregon.Clock.now())

  def current_membership(%User{} = user, now) do
    memberships = Ecto.assoc(user, :memberships)

    from(m in memberships,
      where: m.starts_at <= ^now and m.expires_at > ^now
    )
    |> Repo.one()
  end

  def current_membership(_, _), do: nil

  def has_current_membership?(user, now \\ TeamOregon.Clock.now()) do
    case current_membership(user, now) do
      %Membership{} -> true
      _ -> false
    end
  end
end
