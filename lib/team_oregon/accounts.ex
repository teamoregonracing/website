defmodule TeamOregon.Accounts do
  @moduledoc """
  Functions dealing with user account operations
  """

  alias TeamOregon.Accounts.{User, Credential}
  alias TeamOregon.{Repo, Email, Mailer}

  def registration_form(attrs \\ %{}) do
    User.registration_changeset(attrs)
  end

  def register_user(registration_details) do
    registration_details
    |> User.registration_changeset()
    |> Repo.insert()
    |> send_email_confirmation()
  end

  def confirm_email_address(email_address, confirmation_code) do
    case find_user_by_email_address(email_address) do
      %{email_confirmation_code: ^confirmation_code} = user ->
        user
        |> User.email_confirmation_changeset()
        |> Repo.update()

      _ ->
        {:error, "incorrect email address and/or confirmation code"}
    end
  end

  def email_is_confirmed?(email_address) do
    user = find_user_by_email_address(email_address)
    user && user.email_confirmed_at
  end

  def get_user(user_id) do
    Repo.get(User, user_id)
  end

  def request_confirmation_email(email_address) do
    if email_is_confirmed?(email_address) do
      :already_confirmed
    else
      if find_user_by_email_address(email_address) do
        send_email_confirmation(email_address)
      end

      :ok
    end
  end

  def sign_in(email_address, password) do
    email_address
    |> find_user_by_email_address()
    |> load_user_credential()
    |> verify_password(password)
  end

  defp send_email_confirmation(email_address) when is_binary(email_address) do
    user = find_user_by_email_address(email_address)

    Email.email_address_confirmation(
      user,
      user.email_confirmation_code,
      confirmation_url(user.email_confirmation_code)
    )
    |> Mailer.deliver_later()
  end

  defp send_email_confirmation(
         {:ok, %{email_confirmation_code: code} = user} = insert_result
       ) do
    user
    |> Email.email_address_confirmation(code, confirmation_url(code))
    |> Mailer.deliver_later()

    insert_result
  end

  defp send_email_confirmation(insert_result), do: insert_result

  defp confirmation_url(confirmation_code) do
    TeamOregonWeb.Router.Helpers.registration_url(
      TeamOregonWeb.Endpoint,
      :input_email_confirmation_code,
      confirmation_code: confirmation_code
    )
  end

  defp find_user_by_email_address(email_address) do
    Repo.get_by(User, email_address: email_address)
  end

  defp load_user_credential(nil), do: nil
  defp load_user_credential(user), do: Repo.preload(user, :credential)

  defp verify_password(nil, _) do
    Credential.verify_password(nil, "")
    :error
  end

  defp verify_password(%User{} = user, password) do
    if Credential.verify_password(user.credential, password) do
      {:ok, user}
    else
      :error
    end
  end
end
