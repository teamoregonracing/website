defmodule TeamOregon.Email do
  import Bamboo.Email

  alias TeamOregon.Accounts

  def email_address_confirmation(
        %Accounts.User{} = user,
        confirmation_code,
        confirmation_url
      ) do
    new_email()
    |> to({user.name, user.email_address})
    |> from({"Team Oregon", "info@teamoregon.org"})
    |> subject("Please Confirm Your Email Address")
    |> text_body(
      String.trim("""
      Hello #{user.name},

      Your email confirmation code is: #{confirmation_code}

      Visit #{confirmation_url} to confirm your email address.

      Your Team Oregon membership account has been created, but you must confirm
      your email address before you may use it.

      *Why am I receiving this message?*

      Someone has used your email address while attempting to create an account
      with Team Oregon. If it wasn't you, you may safely ignore this message;
      the account will not be activated without the above confirmation.

      -- 
      Regards,
      Team Oregon
      https://teamoregon.org
      """)
    )
  end
end
