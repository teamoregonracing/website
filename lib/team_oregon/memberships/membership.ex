defmodule TeamOregon.Memberships.Membership do
  use Ecto.Schema

  import Ecto.Changeset

  import Ecto.Query

  alias TeamOregon.Memberships.{MembershipType, Payment}
  alias TeamOregon.Accounts.User

  schema "memberships" do
    belongs_to(:user, User)
    belongs_to(:membership_type, MembershipType)
    embeds_one(:payment_data, Payment)
    field :starts_at, :utc_datetime_usec
    field :expires_at, :utc_datetime_usec
    timestamps()
  end

  def changeset(
        %{
          user: _user,
          membership_type: _membership_type,
          payment_data: _payment_data
        } = changes
      ) do
    %__MODULE__{}
    |> change(changes)
  end
end
