defmodule TeamOregon.Memberships.MembershipType do
  use Ecto.Schema

  import Ecto.Changeset

  schema "membership_types" do
    field :name, :string
    field :price, :decimal
    field :approval_required, :boolean
  end

  def changeset(attrs) do
    %__MODULE__{}
    |> cast(attrs, [:name, :price, :approval_required])
    |> validate_required([:name, :price])
    |> unique_constraint(:name)
  end
end
