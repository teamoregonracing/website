defmodule TeamOregon.Accounts.User do
  use Ecto.Schema

  import Ecto.Changeset

  alias TeamOregon.Accounts.Credential
  alias TeamOregon.Memberships.Membership

  schema "users" do
    field :email_address, :string
    field :name, :string
    field :display_name, :string
    field :email_confirmation_code, :string
    field :email_confirmed_at, :utc_datetime_usec
    has_one :credential, Credential
    has_many :memberships, Membership

    field :accepts_code_of_conduct, :boolean, virtual: true

    timestamps()
  end

  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email_address, :name, :display_name])
    |> validate_required([:email_address, :name, :display_name])
    |> unsafe_validate_unique([:email_address], TeamOregon.Repo)
    |> unsafe_validate_unique([:display_name], TeamOregon.Repo)
    |> unique_constraint(:email_address)
    |> unique_constraint(:display_name)
  end

  def email_confirmation_changeset(user) do
    user
    |> change
    |> put_change(:email_confirmed_at, DateTime.utc_now())
    |> put_change(:email_confirmation_code, nil)
  end

  def registration_changeset(attrs) do
    %__MODULE__{}
    |> changeset(attrs)
    |> cast(attrs, [:accepts_code_of_conduct])
    |> validate_required([:accepts_code_of_conduct])
    |> cast_assoc(:credential, with: &Credential.changeset/2, required: true)
    |> put_email_confirmation_code
  end

  defp put_email_confirmation_code(changeset) do
    case changeset do
      %{changes: %{email_address: email_address}} ->
        put_change(
          changeset,
          :email_confirmation_code,
          generate_confirmation_code(email_address)
        )

      _ ->
        changeset
    end
  end

  defp generate_confirmation_code(email_address) do
    :secret_key_base
    |> TeamOregon.get_config()
    |> (&:crypto.hash(:md5, &1 <> email_address)).()
    |> Base.encode16()
  end
end
