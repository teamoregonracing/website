defmodule TeamOregon.Accounts.Credential do
  use Ecto.Schema

  import Ecto.Changeset

  alias TeamOregon.Accounts.User

  schema "credentials" do
    field :password_hash, :binary
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true

    belongs_to :user, User

    timestamps()
  end

  def changeset(credential, attrs) do
    credential
    |> cast(attrs, [:password, :password_confirmation])
    |> validate_required([:password, :password_confirmation])
    |> validate_confirmation(:password)
    |> validate_length(:password, min: 8, max: 100)
    |> put_pass_hash()
  end

  def verify_password(credential, password) do
    case Comeonin.Pbkdf2.check_pass(credential, password) do
      {:ok, _} -> true
      {:error, _} -> false
    end
  end

  defp put_pass_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        put_change(changeset, :password_hash, Comeonin.Pbkdf2.hashpwsalt(pass))

      _ ->
        changeset
    end
  end
end
