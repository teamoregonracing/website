defmodule TeamOregon do
  @moduledoc """
  TeamOregon keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  def get_config(key) do
    case Application.get_env(:team_oregon, key) do
      {:system, key, default} ->
        System.get_env(key) || default

      {:system, key} ->
        System.get_env(key)

      nil ->
        raise "#{key} has not been configured for the :team_oregon app"

      value ->
        value
    end
  end
end
