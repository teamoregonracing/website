defmodule TeamOregon.Repo.Migrations.AddMembershipsTable do
  use Ecto.Migration

  def change do
    execute("CREATE EXTENSION btree_gist", "DROP EXTENSION btree_gist")

    create table(:memberships) do
      add :user_id, references(:users, on_delete: :delete_all, on_update: :update_all)

      add :membership_type_id,
          references(:membership_types, on_delete: :restrict, on_update: :update_all)

      add :payment_data, :map, null: false
      add :starts_at, :utc_datetime_usec
      add :expires_at, :utc_datetime_usec

      timestamps()
    end

    create index(:memberships, [:user_id, :starts_at, :expires_at])
    create constraint(:memberships, :no_overlaps, exclude: ~s|GIST (
      user_id WITH =,
      TSRANGE(starts_at, expires_at) WITH &&
    )|)
  end
end
