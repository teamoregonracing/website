defmodule TeamOregon.Repo.Migrations.AddUsersAndCredentials do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :email_address, :string, null: false
      add :name, :string, null: false
      add :display_name, :string, null: false
      add :email_confirmation_code, :string
      add :email_confirmed_at, :utc_datetime

      timestamps()
    end

    create table(:credentials) do
      add :password_hash, :binary, null: false
      add :user_id, references(:users, on_delete: :delete_all, null: false)

      timestamps()
    end

    create unique_index(:users, [:email_address])
    create unique_index(:users, [:display_name])
  end
end
