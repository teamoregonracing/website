defmodule TeamOregon.Repo.Migrations.AddMembershipTypes do
  use Ecto.Migration

  def change do
    create table(:membership_types) do
      add :name, :string, null: false
      add :price, :decimal, null: false
      add :approval_required, :boolean, null: false, default: false
    end

    create index(:membership_types, :name, unique: true)
  end
end
